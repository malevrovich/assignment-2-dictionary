%include "lib.inc"

global find_word

section .text

find_word:
    push rdi
    push rsi

    add rsi, 8
    call string_equals

    pop rsi
    pop rdi
    
    test rax, rax
    jnz  .found

    .go_next:
        cmp qword[rsi], 0
        jz .end_of_list
        mov rsi, [rsi]
        jmp find_word

    .found:
        mov rax, rsi
        ret
        
    .end_of_list:
        xor rax, rax
        ret