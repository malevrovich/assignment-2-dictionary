global exit
global string_length
global print_string
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop

    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err:
    mov rsi, rdi
    mov rdi, qword 2
    jmp print_string_at

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    mov rdi, qword 1
    jmp print_string_at

; Печатает строку в определенный fd, принимает fd и адрес на начало нуль терминированной строки
print_string_at:
    push rdi
    push rsi
    mov rdi, rsi
    call string_length
    pop rsi
    pop rdi

    test rax, rax
    je .end

    mov rdx, rax
    mov rax, 1
    syscall
    .end:
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi

    ; null terminator
    dec rsp
    mov [rsp], byte 0
    
    xor rcx, rcx
    mov r9, 10
    .calc_loop:
    ; rax = rax / 10; rdx = rdx % 10
    xor rdx, rdx
    div r9

    add rdx, '0'

    ; saving result
    dec rsp
    mov [rsp], dl

    ; count bytes on stack
    inc rcx

    ; while rax != 0
    test rax, rax
    jne .calc_loop

    mov rdi, rsp    
    push rcx
    call print_string
    pop rcx

    inc rsp
    add rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    cmp rax, 0
    jge .exec

    mov rdi, '-'
    push rax
    call print_char
    pop rax
    neg rax

    .exec:
    mov rdi, rax
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx

    .loop:
    mov dl, byte [rdi + rcx]
    cmp dl, byte [rsi + rcx]
    jne .bad
    test dl, dl
    jz .good
    inc rcx
    jmp .loop

    .good:
    mov rax, 1
    ret
    .bad:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push byte 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    pop rax
    jnz .good
    mov rax, 0
    .good:
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    xor rcx, rcx

    dec rsi
    
    mov r12, 1 ; start of word flag
    .read_loop:
    push rsi
    push rdi
    push rcx
    call read_char
    pop rcx
    pop rdi
    pop rsi

    .check_eof:
    ; if rax == 0 ==> EOF ==> end
    test rax, rax
    je .end

    .check_whitespace:
    cmp rax, 0x20
    je .cut_or_stop
    cmp rax, 0x9
    je .cut_or_stop
    cmp rax, 0xA
    je .cut_or_stop

    .write:
    ; if rcx >= rsi - 1 ==> end of buffer
    mov r12, 0
    cmp rcx, rsi
    jge .error

    mov [rdi + rcx], al
    inc rcx

    jmp .read_loop

    .cut_or_stop:
    ; if start of word flag ==> continue
    cmp r12, 1
    jz .read_loop
    ; else stop

    .end:
    xor rax, rax
    mov [rdi + rcx], al
    mov rax, rdi
    mov rdx, rcx
    pop r12
    ret
    
    .error:
    mov rax, 0
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx

    mov r9, 10
    .start:
    cmp [rdi + rcx], byte '0'
    jl .end_of_digits
    cmp [rdi + rcx], byte '9'
    jg .end_of_digits
    
    mul r9
    mov dl, [rdi + rcx]
    sub rdx, '0'
    add rax, rdx
    
    inc rcx

    jmp .start

    .end_of_digits:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    
    cmp [rdi], byte '-'
    je .signed_negative
    cmp [rdi], byte '+'
    je .signed_positive
    
    .unsigned:
    jmp parse_uint

    .signed_positive:
    inc rdi
    call parse_uint
    inc rdx
    ret

    .signed_negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx

    .loop:
    mov r8, [rdi + rcx]
    cmp r8, byte 0
    je .end
    cmp rcx, rdx
    jge .eob
    mov [rsi + rcx], r8
    inc rcx
    jmp .loop

    .eob:
    mov rax, 0
    ret

    .end:
    mov rax, rcx
    ret