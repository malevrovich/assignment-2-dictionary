global _start

%include "lib.inc"
extern find_word

%assign BUFFER_SIZE (255 + 1)

section .bss
input_buffer: RESB BUFFER_SIZE

section .rodata
%include "words.inc"

%ifdef LIST_HEAD
%else
    %error "Initialize list before using"
%endif

read_error_msg: db "Read error, check size of your str and BUFF", 10, 0
not_found_error_msg: db "Not found!", 10, 0

section .text

_start:
    mov rdi, input_buffer
    mov rsi, BUFFER_SIZE
    
    call read_word

    test rax, rax
    
    jz .read_error

    mov rdi, rax
    mov rsi, LIST_HEAD

    call find_word

    test rax, rax
    jz .not_found_error

    mov rdi, rax
    push rdi
    add rdi, 8
    call string_length
    pop rdi
    add rdi, rax
    add rdi, 9
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

    .read_error:
        mov rdi, read_error_msg
        call print_err
        call exit

    .not_found_error:
        mov rdi, not_found_error_msg
        call print_err
        call exit
