%macro colon 2
%ifid %2
    %2:
        %ifdef LIST_HEAD
        dq LIST_HEAD
    %else
        dq 0
    %endif

    %ifstr %1
        db %1, 0
    %else
        %error "Only string allowed as first argument of colon macro"
    %endif
%else
    %error "Second argument of colon macro must be identifier"       
%endif

%define LIST_HEAD %2
%endmacro